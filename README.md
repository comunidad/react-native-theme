# Fork Shoutem

Repositorio dentro de nuestra organización, actualizable desde shoutem github y personalizable para nuestras necesidades


# Registro de cambios

- Se remplazo Mocha Test por Jest Test, Dependencias
- Se Migro las pruebas de Mocha a Jest, Files

![alt text](http://jlord.us/git-it/assets/imgs/clone.png)

# Theme
 
The React Native component's style is usually defined as a static variable along with the component itself. This makes it easy to build self contained components that always look and behave in the same way. On the other hand, it complicates building themeable (or skinnable) components that could have multiple styles that can be customized without touching the component source code.
`@corvus/react-native-theme` is built to address that problem. With themes, you can target specific components in your app and customize them through one file, just like you would do it with CSS on the web.

## Install


```bash
$ npm install --save @corvus/react-native-theme
```

## Docs

All the documentation is available on the [Developer portal](http://corvus.github.io/docs/ui-toolkit/theme/introduction).

## Community

Join [our community](https://www.facebook.com/groups/corvus.community/) on Facebook. Also, feel free to ask a question on Stack Overflow using ["corvus" tag](http://stackoverflow.com/tags/corvus).

## Examples

Create new React Native project:

```bash
$ react-native init HelloWorld
```

Install `@corvus/ui` and `@corvus/react-native-theme` and link them in your project:

```bash
$ cd HelloWorld
$ npm install --save @corvus/ui
$ npm install --save @corvus/react-native-theme
$ rnpm link
```

Now, simply copy the following to your `index.ios.js` file of React Native project:

```JavaScript
import React, { Component } from 'react';
import { AppRegistry, Dimensions } from 'react-native';
import { StyleProvider } from '@corvus/react-native-theme';
import {
  Card,
  Image,
  View,
  Subtitle,
  Caption,
} from '@corvus/ui';

const window = Dimensions.get('window');

const Colors = {
  BACKGROUND: '#ffffff',
  SHADOW: '#000000',
};

const MEDIUM_GUTTER = 15;

const theme = {
  'corvus.ui.View': {
    '.h-center': {
      alignItems: 'center',
    },

    '.v-center': {
      justifyContent: 'center',
    },

    '.flexible': {
      flex: 1,
    },

    flexDirection: 'column',
  },

  'corvus.ui.Card': {
    'corvus.ui.View.content': {
      'corvus.ui.Subtitle': {
        marginBottom: MEDIUM_GUTTER,
      },

      flex: 1,
      alignSelf: 'stretch',
      padding: 10,
    },

    width: (180 / 375) * window.width,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: Colors.BACKGROUND,
    borderRadius: 2,
    shadowColor: Colors.SHADOW,
    shadowOpacity: 0.1,
    shadowOffset: { width: 1, height: 1 },
  },

  'corvus.ui.Image': {
    '.medium-wide': {
      width: (180 / 375) * window.width,
      height: 85,
    },

    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    resizeMode: 'cover',
    backgroundColor: Colors.BACKGROUND,
  },
};

class HelloWorld extends Component {
  render() {
    return (
      <StyleProvider style={theme}>
        <View styleName="flexible vertical v-center h-center">
          <Card>
            <Image
              styleName="medium-wide"
              source={{ uri: 'http://corvus.github.io/img/ui-toolkit/examples/image-12.png' }}
            />
            <View styleName="content">
              <Subtitle numberOfLines={4}>
                Lady Gaga Sings National Anthem at Super Bowl 50
              </Subtitle>
              <Caption>21 hours ago</Caption>
            </View>
          </Card>
        </View>
      </StyleProvider>
    );
  }
}

AppRegistry.registerComponent('HelloWorld', () => HelloWorld);
```

Finally, run the app!

```bash
$ react-native run-ios
```


