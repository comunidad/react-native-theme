import React from 'react-native';
import { mount } from 'enzyme';
import { Theme } from '../';
import StyleProviderTestAppComponent from './mocks/StyleProviderTestAppComponent';
import StyleProviderTestComponent from './mocks/StyleProviderTestComponent';

describe('StyleProvider', () => {
  it('provides a theme', () => {
    const demo = mount(
      <StyleProviderTestAppComponent>
        <StyleProviderTestComponent />
      </StyleProviderTestAppComponent>
    );
    const passedTheme = demo.find(StyleProviderTestComponent).nodes[0].getThemeStyle();

    expect(passedTheme instanceof Theme).toBe(true);
  });
});
