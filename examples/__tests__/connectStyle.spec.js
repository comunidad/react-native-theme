import React, { Component } from 'react';
import { mount } from 'enzyme';
import { TEST_PROPERTY } from './mocks/ThemeTest';
import StyleProviderTestAppComponent,
{
  TEST_VARIABLE,
} from './mocks/StyleProviderTestAppComponent';
import {
  ConnectedClassComponent,
  ConnectedStatelessComponent,
  componentName,
} from './mocks/ConnectStyleTestComponents';

describe('connectStyle', () => {
  it('provides proper style to component', () => {
    const demo = mount(
      <StyleProviderTestAppComponent>
        <ConnectedClassComponent />
      </StyleProviderTestAppComponent>
    );
    const passedStyle = demo.find(ConnectedClassComponent)
      .nodes[0].refs.wrappedInstance.props.style;

    expect(passedStyle.testStyle.testProperty).toEqual(TEST_PROPERTY);
    expect(passedStyle.testStyle.variableProperty).toEqual(TEST_VARIABLE);
  });
  it('provides normalized style ', () => {
    const denormalizedStyle = { denormalized: { padding: 5 } };
    const demo = mount(
      <StyleProviderTestAppComponent>
        <ConnectedClassComponent style={denormalizedStyle} />
      </StyleProviderTestAppComponent>
    );

    const passedStyle = demo.find(ConnectedClassComponent)
      .nodes[0].refs.wrappedInstance.props.style;

    const normalizedStyle = {
      paddingTop: 5,
      paddingBottom: 5,
      paddingLeft: 5,
      paddingRight: 5,
    };

    expect(passedStyle.denormalized).toEqual(normalizedStyle);
    expect(passedStyle.testStyle.variableProperty).toEqual(TEST_VARIABLE);
  });
  it('creates ref for react Component component', () => {
    const demo = mount(
      <StyleProviderTestAppComponent>
        <ConnectedClassComponent />
      </StyleProviderTestAppComponent>
    );
    const instance = demo.find(ConnectedClassComponent)
      .nodes[0].refs.wrappedInstance;

    expect(instance instanceof Component).toBeTruthy();
  });
  it('doesn\'t create ref for stateless component', () => {
    const demo = mount(
      <StyleProviderTestAppComponent>
        <ConnectedStatelessComponent />
      </StyleProviderTestAppComponent>
    );
    const instance = demo.find(ConnectedStatelessComponent)
      .nodes[0].refs.wrappedInstance;

    expect(instance).toBeFalsy();
  });
  describe('virtual', () => {
    it('pass parent style to child component as parent style', () => {
      const context = {
        parentStyle: {
          'some.child.component': {
            flex: 1,
          },
          test: 1,
        },
      };
      const demo = mount(<ConnectedClassComponent virtual/>, { context });
      const instanceContext = demo.instance().getChildContext();

      expect(instanceContext.parentStyle).toBe(context.parentStyle);
    });
    it('doesn\'t pass parent style to child component as parent style', () => {
      const context = {
        parentStyle: {
          [componentName]: {
            'some.child.component': {
              flex: 1,
            },
            'some.child.component1': {
              flex: 2,
            },
            flex: 1,
          },
        },
      };
      const demo = mount(<ConnectedClassComponent />, { context });
      const instanceContext = demo.instance().getChildContext();

      const expectedParentStyle = {
        'some.child.component': {
          flex: 1,
        },
        'some.child.component1': {
          flex: 2,
        },
      };
      expect(instanceContext.parentStyle).toEqual(expectedParentStyle);
    });
  });
});
